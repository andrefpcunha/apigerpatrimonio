﻿using System.Collections.Generic;
using System.Net;
using ApiGerPatrimonio.Models;
using ApiGerPatrimonio.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace ApiGerPatrimonio.Controllers
{
    [Route("api/[controller]")]
    public class MarcasController : Controller
    {
        private readonly IMarcaService service;

        public MarcasController(IMarcaService service)
        {
            this.service = service;
        }




        // GET api/marcas
        [HttpGet]
        public List<Marca> Get()
        {
            var model = service.GetMarcas();

            return model;
        }

        // GET api/marcas/1
        [HttpGet("{id}")]
        public Marca GetMarca(int id)
        {
            var item = service.GetMarcaById(id);

            return item;
        }

        // POST api/marcas
        [HttpPost]
        public HttpStatusCode Post([FromBody]JObject body)
        {
            string nome = service.GetMarcaByNome(body.GetValue("Nome").Value<string>());

            if (nome == body.GetValue("Nome").Value<string>())
                //Não permite salvar a Marca com o mesmo nome
                return HttpStatusCode.Ambiguous;

            var item = new Marca()
            {
                Nome = body.GetValue("Nome").Value<string>()
            };

            service.SalvarMarca(item);

            return HttpStatusCode.OK;
        }

        // PUT api/marcas/1
        [HttpPut("{id}")]
        public HttpStatusCode Put(int id, [FromBody]JObject body)
        {
            var resposta = service.GetMarcaById(id);

            if (resposta == null)
                return HttpStatusCode.NotFound;

            else
            {
                var item = new Marca()
                {
                    MarcaID = id,
                    Nome = body.GetValue("Nome").Value<string>()                    
                };

                service.ModificarMarca(item);

                return HttpStatusCode.OK;
            }
        }

        // DELETE api/marcas/1
        [HttpDelete("{id}")]
        public HttpStatusCode Delete(int id)
        {
            var item = service.GetMarcaById(id);


            if (item == null)
            {
                return HttpStatusCode.NotFound;
            }

            service.DeleteMarca(item);


            return HttpStatusCode.OK;
        }
    }
}