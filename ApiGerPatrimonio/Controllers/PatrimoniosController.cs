﻿using System.Collections.Generic;
using System.Net;
using ApiGerPatrimonio.Models;
using ApiGerPatrimonio.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace ApiGerPatrimonio.Controllers
{
    [Route("api/[controller]")]
    public class PatrimoniosController : Controller
    {
        private readonly IPatrimonioService service;

        public PatrimoniosController(IPatrimonioService service)
        {
            this.service = service;
        }




        // GET api/patrimonios
        [HttpGet]
        public List<Patrimonio> Get()
        {
            var model = service.GetPatrimonios();

            return model;
        }

        // GET api/patrimonios/1
        [HttpGet("{id}")]
        public Patrimonio GetPatrimonio(int id)
        {
            var item = service.GetPatrimonioById(id);

            return item;
        }

        // POST api/patrimonios
        [HttpPost]
        public HttpStatusCode Post([FromBody]JObject body)
        {
            var item = new Patrimonio()
            {
                Nome = body.GetValue("Nome").Value<string>(),
                Descricao = body.GetValue("Descricao").Value<string>(),
                MarcaID = body.GetValue("MarcaID").Value<int>()
            };

            service.SalvarPatrimonio(item);

            return HttpStatusCode.OK;
        }

        // PUT api/patrimonios/1
        [HttpPut("{id}")]
        public HttpStatusCode Put(int id, [FromBody]JObject body)
        {
            var resposta = service.GetPatrimonioById(id);

            if (resposta == null)
                return HttpStatusCode.NotFound;

            else
            {
                var item = new Patrimonio()
                {
                    NrTombo = id,
                    Nome = body.GetValue("Nome").Value<string>(),
                    Descricao = body.GetValue("Descricao").Value<string>(),
                    MarcaID = body.GetValue("MarcaID").Value<int>()
                };

                service.ModificarPatrimonio(item);

                return HttpStatusCode.OK;
            }
        }

        // DELETE api/patrimonios/1
        [HttpDelete("{id}")]
        public HttpStatusCode Delete(int id)
        {
            var item = service.GetPatrimonioById(id);

            
            if (item == null)
            {
                return HttpStatusCode.NotFound;
            }

            service.DeletePatrimonio(item);


            return HttpStatusCode.OK;
        }
    }
}
