﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiGerPatrimonio.Models
{
    public class Patrimonio
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NrTombo { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }

       public int MarcaID { get; set; }

    }
}
