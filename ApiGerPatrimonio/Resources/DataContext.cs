﻿using Microsoft.EntityFrameworkCore;
using ApiGerPatrimonio.Models;

namespace ApiGerPatrimonio.Resources
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        { }

        public DbSet<Patrimonio> Patrimonio { get; set; }

        public DbSet<Marca> Marca { get; set; }
    }
}
