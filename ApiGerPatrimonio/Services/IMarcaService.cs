﻿using ApiGerPatrimonio.Models;
using System.Collections.Generic;

namespace ApiGerPatrimonio.Services
{
    public interface IMarcaService
    {
        List<Marca> GetMarcas();

        Marca GetMarcaById(int id);

        void SalvarMarca(Marca item);

        void ModificarMarca(Marca item);

        void DeleteMarca(Marca item);

        string GetMarcaByNome(string nome);
    }
}
