﻿using ApiGerPatrimonio.Models;
using System.Collections.Generic;

namespace ApiGerPatrimonio.Services
{
    public interface IPatrimonioService
    {
        List<Patrimonio> GetPatrimonios();

        Patrimonio GetPatrimonioById(int id);

        void SalvarPatrimonio(Patrimonio item);

        void ModificarPatrimonio(Patrimonio item);

        void DeletePatrimonio(Patrimonio item);
    }
}
