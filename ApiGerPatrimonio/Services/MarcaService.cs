﻿using System.Collections.Generic;
using System.Linq;
using ApiGerPatrimonio.Models;
using ApiGerPatrimonio.Resources;
using Microsoft.EntityFrameworkCore;

namespace ApiGerPatrimonio.Services
{
    public class MarcaService : IMarcaService
    {
        private readonly DataContext _context;
        public MarcaService(DataContext context)
        {
            _context = context;
        }


        public List<Marca> GetMarcas()
        {
            return _context.Marca.ToList();
        }

        public Marca GetMarcaById(int id)
        {
            return _context.Marca.Where(m => m.MarcaID == id).FirstOrDefault();
        }

        public void SalvarMarca(Marca item)
        {
            _context.Marca.Add(item);
            _context.SaveChanges();
        }

        public void ModificarMarca(Marca item)
        {
            var local = _context.Set<Marca>()
            .Local
            .FirstOrDefault(entry => entry.MarcaID.Equals(item.MarcaID));

            if (local != null)
            {
                // detach: para evitar conflito de chaves
                _context.Entry(local).State = EntityState.Detached;
            }


            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void DeleteMarca(Marca item)
        {
            _context.Marca.Remove(item);
            _context.SaveChanges();
        }

        public string GetMarcaByNome(string nome)
        {
            return (from p in _context.Marca.Where(m => m.Nome == nome)
                    select p.Nome).FirstOrDefault();

        }
    }
}
