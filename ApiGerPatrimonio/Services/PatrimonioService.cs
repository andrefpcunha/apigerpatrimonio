﻿using System.Collections.Generic;
using System.Linq;
using ApiGerPatrimonio.Models;
using ApiGerPatrimonio.Resources;
using Microsoft.EntityFrameworkCore;

namespace ApiGerPatrimonio.Services
{
    public class PatrimonioService : IPatrimonioService
    {

        private readonly DataContext _context;
        public PatrimonioService(DataContext context)
        {
            _context = context;
        }
        

        public List<Patrimonio> GetPatrimonios()
        {
            return _context.Patrimonio.ToList();
        }

        public Patrimonio GetPatrimonioById(int id)
        {
            return _context.Patrimonio.Where(m => m.NrTombo == id).FirstOrDefault();
        }

        public void SalvarPatrimonio(Patrimonio item)
        {
            _context.Patrimonio.Add(item);
            _context.SaveChanges();
        }

        public void ModificarPatrimonio(Patrimonio item)
        {
            var local = _context.Set<Patrimonio>()
            .Local
            .FirstOrDefault(entry => entry.NrTombo.Equals(item.NrTombo));

            if (local != null)
            {
                // detach: para evitar conflito de chaves
                _context.Entry(local).State = EntityState.Detached;
            }
            

            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void DeletePatrimonio(Patrimonio item)
        {
            _context.Patrimonio.Remove(item);
            _context.SaveChanges();
        }
    }
}
