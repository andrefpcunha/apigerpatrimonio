# WEB API - NET CORE 2.0
##### CRUD de API REST - Exemplo básico para gestão de patrimônios
###### Sobre o autor: [LinkedIN](https://linkedin.com/in/andrecunha-softeng)
(https://gitlab.com/andrefpcunha/apigerpatrimonio)


A API foi desenvolvida utilizando conceitos elementares do ASP.NET Core 2.0, EntityFrameworkCore, DependencyInjection, Linq e padrão e roteamento MVC.

## Pré-requisitos
  - Visual Studio 2017 versão 15.9.3 ou posterior
  - SDK 2.0 ou posterior do .NET Core
  - Sql Server 2008 R2 ou posterior
  - Postman

## Requisitos técnicos

  - Padrão MVC
  - Injeção de Dependências de 'Services'
  - Padrão Linq
  - Padrão REST
  - Endpoints no formato JSON


### Instalação
```sh
1 Abra o "SQL SERVER MANEGEMENT STUDIO", conecte ao seu servidor "Localhost"
3 Clique no botão "NOVA CONSULTA" usando a base de dados "master"
4 Abra e Execute o arquivo "scriptDataBase.SQL" para criar a estrutura da base de dados
5 Abra o arquivo "ApiGerPatrimonio.sln" usando o "Visual Studio 2017"
6 Execute o projeto utilizando a tecla "F5"(teclado)
```


### Estrutura da API

| API | Descrição |
| ------ | ------ |
| GET /api/patrimonios | Obter todos os Patrimônios|
| GET /api/marcas | Obter todos as Marcas|
| GET /api/patrimonios/{id} | Obter um Patrimônio por nº do tombo |
| GET /api/marcas/{id} | Obter um Marca por MarcaID |
| POST /api/patrimonios | Adicionar um novo Patrimônio |
| POST /api/marcas | Adicionar uma nova Marca |
| PUT /api/patrimonios/{id} | Atualizar um Patrimônio |
| PUT /api/marcas/{id} | Atualizar uma Marca |
| DELETE /api/patrimonios/{id} | Excluir um Patrimônio |
| DELETE /api/marcas/{id} | Excluir uma Marca |


### Testar a API
 Abra o [POSTMAN] e envie(send) a URL, conforme exemplo abaixo:
```sh
[GET] https://localhost:<port>/api/patrimonios
[GET] https://localhost:<port>/api/patrimonios/1
```

Para [verbos] que fazem requisição usando o "Body", utilize o seguinte exemplo:
```sh
[POST] https://localhost:<port>/api/patrimonios
```
Abaixo da [url], selecione a opção [BODY], marque a opção [raw] e, ao lado, especifique o formato [JSON (aplication/json)]
No campo de texto abaixo, utilize o seguinte padrão, como exemplo:
``
{
	"Nome":"Patrimonio X",
	"Descricao": "xxxx xxxxx xxxx xxx",
	"MarcaID":1
}
``





